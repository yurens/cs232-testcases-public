/*
1. Checks that CHA is not used because  class C has a method that matches,
    but it is never instantiated. Therefore one should not see Entry_main->B.M();
2. Checks that the code is not checking guard conditions on control statements (e.g. if)
    because the else statement in main should never be called, but should show up in 0CFA
3. Checks to see that it can check if there is an overridden method and when not it correctly reverts to the base class
    -And in a derived class that it still knows the correct base class implementation.
4. Checks that the returns types are being included incstraints otherwise res.invoke will not resolve correctly
5. Makes sure that the an argument could be a return type which will generate a special type of constraint

*/

class Main
{
    public static void main (String[] args)
    {
        A temp;
        A res;
        int i;
        if(1 < 1)
        {
            temp = new A();
            res = temp.returnSelf(temp);
            i = res.invoke();
        }
        else
        {
            temp = new B();
            res = temp.returnSelf(temp);
            i = res.invoke();
        }
    }
}

class A
{
    public A returnSelf(A arg)
    {
        int t;
        t = arg.action();
        return arg;
    }
    public int invoke()
    {
        return 1;
    }
    public int action()
    {
        return 0;
    }
}
class B extends A
{
    public A returnSelf(A arg)
    {
        int t;
        t = arg.action();
        return arg;
    }
    public int invoke()
    {
        return 2;
    }
}
class C extends B
{
    public A returnSelf(A arg)
    {
        int t;
        t = arg.action();
        return arg;
    }
    public int invoke()
    {
        return 3;
    }
}