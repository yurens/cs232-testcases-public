// Test that "this" is treated as implicit formal parameter.
class Main {
    public static void main(String[] a) {
        System.out.println(new A().AOnly());
        System.out.println(new A().AOnly2());
        System.out.println(new B().AOnly());
     }
}


class A {
    public int AOnly() {
        return this.Virt();
    }

    public int AOnly2() {
        return this.Virt();
    }

    public int Virt() {
        return 2;
    }
}

class B extends A {
    public int Virt() {
        return 3;
    }
}